<?php
/**
 * Field Subtitle special layout, built on top of
 * @file field--fences-div.tpl.php
 * Wrap each field value in the <div> element.
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-div-element
 */
?>

<?php foreach ($items as $delta => $item): ?>
  <div class="<?php print $classes; ?>"<?php print $attributes; ?>>
    <div class="wrapper"><?php print render($item); ?></div>
  </div>
<?php endforeach; ?>
