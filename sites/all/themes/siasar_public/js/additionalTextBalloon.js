(function ($) {
  'use strict';

  $(document).ready(function () {

    var $textContainer = $('.field-extra-text');

    $textContainer.on('click tap', toggleBalloon);

    function toggleBalloon() {
      var that = this;
      var $this = $(this);

      $textContainer.not($this).removeClass('active');
      $this.toggleClass('active');
    }

  });

})(jQuery);
