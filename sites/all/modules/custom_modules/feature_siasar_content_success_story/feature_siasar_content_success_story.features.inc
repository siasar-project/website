<?php
/**
 * @file
 * feature_siasar_content_success_story.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_siasar_content_success_story_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feature_siasar_content_success_story_node_info() {
  $items = array(
    'success_story' => array(
      'name' => t('Success story'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
