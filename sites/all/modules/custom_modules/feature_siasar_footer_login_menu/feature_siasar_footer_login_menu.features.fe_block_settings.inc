<?php
/**
 * @file
 * feature_siasar_footer_login_menu.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_siasar_footer_login_menu_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['menu-menu-logins'] = array(
    'cache' => -1,
    'css_class' => 'footer-menu',
    'custom' => 0,
    'delta' => 'menu-logins',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'mothership' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mothership',
        'weight' => 0,
      ),
      'siasar_public' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'siasar_public',
        'weight' => -11,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
