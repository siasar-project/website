<?php
/**
 * @file
 * feature_siasar_footer_login_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_siasar_footer_login_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-logins.
  $menus['menu-logins'] = array(
    'menu_name' => 'menu-logins',
    'title' => 'Logins',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Logins');

  return $menus;
}
