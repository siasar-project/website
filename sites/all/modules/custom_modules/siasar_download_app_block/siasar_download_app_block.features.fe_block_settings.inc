<?php
/**
 * @file
 * siasar_download_app_block.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function siasar_download_app_block_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-download_app'] = array(
    'cache' => -1,
    'css_class' => 'download-app',
    'custom' => 0,
    'i18n_block_language' => array(),
    'i18n_mode' => 1,
    'machine_name' => 'download_app',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'mothership' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mothership',
        'weight' => 0,
      ),
      'siasar_public' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'siasar_public',
        'weight' => -7,
      ),
    ),
    'title' => 'Download the app',
    'visibility' => 1,
  );

  return $export;
}
