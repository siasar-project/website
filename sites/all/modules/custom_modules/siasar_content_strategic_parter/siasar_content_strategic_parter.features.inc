<?php
/**
 * @file
 * siasar_content_strategic_parter.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function siasar_content_strategic_parter_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function siasar_content_strategic_parter_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function siasar_content_strategic_parter_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: strategic_partners
  $nodequeues['strategic_partners'] = array(
    'name' => 'strategic_partners',
    'title' => 'Strategic partners',
    'subqueue_title' => '',
    'size' => 0,
    'link' => 'Add to partners list',
    'link_remove' => 'remove from partners list',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 1,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => 0,
    'i18n' => 1,
    'subqueues' => 1,
    'types' => array(
      0 => 'partner',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_node_info().
 */
function siasar_content_strategic_parter_node_info() {
  $items = array(
    'partner' => array(
      'name' => t('Strategic partner'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
